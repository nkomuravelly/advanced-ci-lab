from logging import getLogger

from django.conf import settings

logger = getLogger(__name__)


def get_feature_flag_service():
    if "client" not in get_feature_flag_service.__dict__:
        logger.debug("Creating Feature Flag service")
        from UnleashClient import UnleashClient
        from environ import Env
        import os

        env = Env()
        env_file = os.path.join(settings.BASE_DIR, 'www/.env')
        if os.path.isfile(env_file):
            env.read_env(env_file=env_file)

        client = UnleashClient(
            url=env('UNLEASH_API_URL'),
            app_name=env('GITLAB_ENVIRONMENT_NAME'),
            instance_id=env('UNLEASH_API_IID'),
            disable_metrics=True,
            disable_registration=True
        )
        get_feature_flag_service.client = client
        logger.debug("Created Feature Flag service")
    else:
        logger.debug("Returning existing Feature Flag Service")

    if not get_feature_flag_service.client.is_initialized:
        logger.debug("Initializing Feature Flag Client")
        get_feature_flag_service.client.initialize_client()
        logger.debug("Feature Flag Client is now initialized")
    else:
        logger.debug("Feature Flag Client had already been initialized")

    return get_feature_flag_service.client
